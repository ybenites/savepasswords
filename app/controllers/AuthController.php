<?php

class AuthController extends \Phalcon\Mvc\Controller
{
    private function _registerSession($user)
    {
        $this->session->set(
            'auth',
            [
                'idUser' => $user->id,
                'userName' => $user->name,
            ]
        );
    }
    public function getLoginAction()
    {
        $this->view->pick('auth/login');
    }
    public function getRegisterAction()
    {
        $this->view->pick('auth/register');
    }
    public function getResetAction()
    {
        $this->view->pick('auth/reset');
    }
    public function postLoginAction()
    {
      
    }
}
