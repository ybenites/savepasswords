<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Save Your Passwords</title>

        
        {{ stylesheet_link("css/materialize.css") }}
        
        {{ stylesheet_link("css/style.min.css") }}
        {{ stylesheet_link("css/custom/custom.min.css") }}
        {{ stylesheet_link("css/layouts/page-center.css") }}


        <!-- <link href="css/style.min.css" type="text/css" rel="stylesheet" media="screen,projection"> -->
        <!-- Custome CSS-->    
        <!-- <link href="css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection"> -->
        <!-- <link href="css/layouts/page-center.css" type="text/css" rel="stylesheet" media="screen,projection"> -->
    </head>
    <body>
        <div class="container">
            {{ content() }}
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        {{ javascript_include("https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js",false) }}

        <!-- Latest compiled and minified JavaScript -->
        {{ javascript_include("js/materialize.js") }}
        <!--plugins.js - Some Specific JS codes for Plugin Settings-->
        {{ javascript_include("js/plugins.min.js") }}
        
        <!--custom-script.js - Add your own theme custom JS-->
        {{ javascript_include("js/custom-script.js") }}
        
    </body>
</html>

