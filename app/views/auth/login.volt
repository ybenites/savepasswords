<!-- Start Page Loading -->
<div id="loader-wrapper">
  <div id="loader"></div>
  <div class="loader-section section-left"></div>
  <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->

<div class="col s12 m6 offset-m3">
  <div class="card-panel">
    <div class="row">
      <form class="col s12" method="POST" action="{{url('login')}}" >
        <input type="hidden" name="token" value="{{ security.getToken() }}">
        <div class="row">
          <div class="input-field col s12 center">
            <img src="images/login-logo.png" alt="" class="circle responsive-img valign profile-image-login">
            <p class="center login-form-text">save your passwords</p>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <input id="username" type="text">
            <label for="username" class="center-align">Username</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="password" type="password">
            <label for="password">Password</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12 m12 l12  login-text">
            <input type="checkbox" id="remember-me" />
            <label for="remember-me">Remember me</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <div class="btn-login">
              <button type="submit" class="btn waves-effect waves-light" name="button">
                Login
              </button>
            </div>
          </div>
        </div>
        <div class="row">
          {#<div class="col s12 m12 l12">#}
            {#<div class="row">#}
              <div class="input-field col s6 m6 l6">
                <p class="medium-small">{{link_to("register","Register Now!")}}</p>
              </div>
              <div class="input-field col s6 m6 l6">
                <p class="right-align medium-small"> {{link_to('reset/password',"Forgot password ?")}}</p>
              {#</div>#}
            {#</div>#}
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
