<?php

use Phalcon\Mvc\Router;

$router = new Router(false);

$router->addGet('/home',
  [
      'controller' => 'index',
      'action' => 'index',
  ]
);
$router->addGet('/',
  [
      'controller' => 'index',
      'action' => 'index',
  ]
);

// Set 404 paths
$router->notFound(
    [
        "controller" => "index",
        "action"     => "route404",
    ]
);

$router->addGet('/login',[
  'controller'=>'auth',
  'action'=>'getLogin'
]);
$router->addPost('/login',[
  'controller'=>'auth',
  'action'=>'postLogin'
]);

$router->addGet('/register',[
  'controller'=>'auth',
  'action'=>'getRegister'
]);

$router->addGet('/reset/password',[
  'controller'=>'auth',
  'action'=>'getReset'
]);


// Route::get('/',[
//   'uses'=>'HomeController@index',
//   'as'=>'home'
//   ]);
//
// Route::get('home', function() {
//     return Redirect::route('home');
// });
//
//
// // Authentication routes...
// Route::get( 'login', [
//   'uses'=>'Auth\AuthController@getLogin',
//   'as' => 'login'
//   ] );
// Route::post( 'login', 'Auth\AuthController@postLogin' );
// Route::get( 'logout', 'Auth\AuthController@getLogout' );
//
// // Registration routes...
// Route::get( 'register', [
//   'uses'=>'Auth\AuthController@getRegister',
//   'as'=>'register'
//   ] );
// Route::post( 'register', 'Auth\AuthController@postRegister' );
//
//
//
// // Password reset link request routes...
// Route::get('password/email',[
//   'uses'=>'Auth\PasswordController@getEmail',
//   'as'=>'email'
//   ]);
// Route::post('password/email', 'Auth\PasswordController@postEmail');
//
// // Password reset routes...
// Route::get('password/reset/{token}',[
//   'uses'=>'Auth\PasswordController@getReset',
//   'as'=>'resetpassword'
//   ]);
// Route::post('password/reset', 'Auth\PasswordController@postReset');
//
//
//
// Route::get('pricing',[
//   'uses'=>'PricingController@index',
//   'as'=>'pricing'
//   ]);
//
//
// Route::post('payment',[
//   'uses'=>'PaymentController@store',
//   'as'=>'payment'
//   ]);
//
//
//
// /*second part student configuration*/
// Route::get('student/dashboard',[
//   'uses'=>'Student\DashboardController@index',
//   'as'=>'studentdashboard'
//   ]);
// Route::get('student/book',[
//   'uses'=>'Student\BookController@index',
//   'as'=>'studentbook'
//   ]);
// Route::get('student/lessons',[
//   'uses'=>'Student\RecordController@index',
//   'as'=>'studentrecord'
//   ]);
// /**/
//
//
// /*third part teacher configuration*/
// Route::get('teacher/dashboard',[
//   'uses'=>'Teacher\DashboardController@index',
//   'as'=>'teacherdashboard'
//   ]);
// Route::get('teacher/book',[
//   'uses'=>'Teacher\BookController@index',
//   'as'=>'teacherbook'
//   ]);
// Route::get('teacher/lessons',[
//   'uses'=>'Teacher\RecordController@index',
//   'as'=>'teacherrecord'
//   ]);


return $router;
